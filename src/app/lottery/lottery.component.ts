import {Component} from '@angular/core';

@Component({
  selector: 'app-lottery',
  templateUrl: './lottery.component.html',
  styleUrls: ['./lottery.component.css']
})

export class LotteryComponent {

  numbers: number[] = [];

  constructor() {
    this.numbers = this.generateNumbers();
  }

  generateNumbers() {
    if (this.numbers){
      this.numbers = [];
    }
    for(let i = 0; i < 5; i++) {
      this.numbers.push(Math.floor(Math.random() * 36 + 1));
    }
    return this.numbers.sort((a, b) => {
      return a - b;
    });
    }
}
