import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LotteryComponent } from './lottery/lottery.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ImgCardComponent } from './img-card/img-card.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    LotteryComponent,
    GalleryComponent,
    ImgCardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
