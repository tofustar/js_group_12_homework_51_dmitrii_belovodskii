import { Component } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {
  showForm = false;
  password = '';
  images = [
    {title: 'Summer', url: 'https://im0-tub-ru.yandex.net/i?id=a7ec200e62161afef36a6531a9e70d05-sr&n=13'},
    {title: 'Autumn', url: 'https://im0-tub-ru.yandex.net/i?id=5e6a6ec403a2da3b61e5ce91e8baf238-sr&n=13'},
    {title: 'Winter', url: 'https://sun9-47.userapi.com/c12746/u12453010/video/l_99157c1f.jpg'},
  ];
  title = '';
  url = '';

  showSome(event: Event){
    const target = <HTMLInputElement>event.target;
    this.password = target.value;
  }

  addCard(event: Event){
    event.preventDefault();
    this.images.push({
      title: this.title,
      url: this.url
    });

  }
}
