import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-img-card',
  templateUrl: './img-card.component.html',
  styleUrls: ['./img-card.component.css']
})
export class ImgCardComponent {
  @Input() title = '';
  @Input() url = '';

  getTitle(){
    return this.title;
  }

  getIcon() {
    return this.url;
  }
}
